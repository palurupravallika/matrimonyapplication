package com.matrimony.demo.Controller;

import java.net.http.HttpHeaders;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.matrimony.demo.Dto.RegisterDTO;
import com.matrimony.demo.Entity.Profile;
import com.matrimony.demo.Entity.Registration;

import com.matrimony.demo.Service.RegisterService;
import com.matrimony.demo.ServiceImp.RegisterServiceImp;


import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/Registrations")

public class RegisterController {

	@Autowired
	private RegisterServiceImp registerServiceImp;

	
  @PostMapping("/register") 
	  public ResponseEntity<Registration> saveRegistration(@Valid @RequestBody Registration register) {
	  
	  Registration savedRegistration =registerServiceImp.saveRegistration(register); 
	  return new ResponseEntity(savedRegistration, HttpStatus.CREATED); 
	  }
	
	
	
	//  @PostMapping("/")
//		public ResponseEntity<?> addregister(@RequestBody Registration register)throws RegistrationFoundException{
//			Optional <Registration> opt = profileRepository1.findByEmail(register.getEmailId());
//			if(opt.isPresent()) {
//				throw new RegistrationFoundException(" Already Registration was done ");
//			}else {
//				registerServiceImp.addregistration(register);
//				return new ResponseEntity<>("registration is successful", HttpStatus.OK);
//			}
//		}


	
	
	@PostMapping("/login")
	public ResponseEntity<String> loginUser(@RequestParam String email, @RequestParam String password) {
		Registration user = registerServiceImp.login(email, password);
		String message;
		if (user != null) {
			message = "Login Successfull";
		}

		else {
			message = "Invalid email.or password";
		}

		return new ResponseEntity<>(message, HttpStatus.OK);

	}
}
