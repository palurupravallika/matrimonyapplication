package com.matrimony.demo.Dto;

import jakarta.persistence.Column;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RegisterDTO {

	@NotNull(message = "name is requred field")
	private String name;
	@NotNull(message = "mail is requred field")
	@Email(message = "invalid Email Id")
	private String emailId;
	@NotNull(message = "mobile number is requred field")
	private String mobileNo;
	@NotNull(message = "password is requred field")

	@Size(min = 8, max = 15)
	private String password;

}
