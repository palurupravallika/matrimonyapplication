package com.matrimony.demo.Service;


import java.util.List;

import com.matrimony.demo.Dto.ProfileDTO;
import com.matrimony.demo.Entity.Profile;


public interface ProfileService {

	Profile addProfile(Profile profile);

	ProfileDTO updateProfile(Integer profileId, ProfileDTO profiledto);

	List<Profile> findProfileByGender(String gender);


}
