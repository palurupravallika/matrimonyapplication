package com.matrimony.demo.Exception;

public class RegistrationFoundException extends Exception {

	public RegistrationFoundException(String message) {
		super(message);

	}

}
