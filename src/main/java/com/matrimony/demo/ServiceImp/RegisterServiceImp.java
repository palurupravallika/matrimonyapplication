package com.matrimony.demo.ServiceImp;


import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;



import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import com.matrimony.demo.Dto.RegisterDTO;
import com.matrimony.demo.Entity.Profile;
import com.matrimony.demo.Entity.Registration;

import com.matrimony.demo.Repository.ProfileRepository;
import com.matrimony.demo.Repository.RegisterRepository;
import com.matrimony.demo.Service.RegisterService;

import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.validation.Valid;

@Service
public class RegisterServiceImp implements RegisterService {

	@Autowired
	private RegisterRepository registerRepository;

	public RegisterRepository getRegisterRepository() {
		return registerRepository;
	}

	public void setRegisterRepository(RegisterRepository registerRepository) {
		this.registerRepository = registerRepository;
	}

	@Override
	public Registration saveRegistration( Registration register) {
		// TODO Auto-generated method stub
	return registerRepository.save(register);
	}
	

	


//	@Override
//	public Registration saveRegistration(Registration register) {
//		
//		return registerRepository.save(register);
//
//	}
//
//	private void validateRegistration(Registration register) {
//
//		if (!isValidEmailFormat(register.getEmail())) {
//			throw new InvalidEmailFormatException("Invalid email format");
//		}
//
//		if (register.getPassword()==null||register.getPassword().length()<5) {
//			throw new InvalidPasswordValidationException("Password must be at least 5 characters");
//
//		}
//	}
//
//	private boolean isValidEmailFormat(String email) {
//		// TODO Auto-generated method stub
//		return true;
//	}
//
//	private boolean isValidPassword(String password) {
//		// TODO Auto-generated method stub
//		return true;
//	}
//	
//	
//	  
//	 
	 
	  

	@Override
	public Registration login(String email, String password) {
		Registration user = registerRepository.findByEmail(email);
		if (user != null && user.getPassword().equals(password)) {
			return user;

		}
		return null;

	}

//	@Override
//	public Registration createRegistration(Registration registration) {
//		// TODO Auto-generated method stub
//		return null;
//	}

	

	
	
}
