package com.matrimony.demo.RegisterServiceTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import static org.mockito.Mockito.when;

import com.matrimony.demo.Entity.Registration;
import com.matrimony.demo.Repository.RegisterRepository;
import com.matrimony.demo.Service.RegisterService;
import com.matrimony.demo.ServiceImp.RegisterServiceImp;

@SpringBootTest
public class RegisterServiceTest {

	@InjectMocks

	private RegisterServiceImp registerService;

	@Mock
	private RegisterRepository registerRepository;

	@Test
	public void testSaveRegistration() {

		// Given
		Registration registration = new Registration();
		registration.setName("Hemanth");
		registration.setEmail("Hemanthp12@gmail.com");
		registration.setMobileNo("9876543210");
		registration.setPassword("password@123");

		when(registerRepository.save(registration)).thenReturn(registration);

		Registration savedRegistration = registerService.saveRegistration(registration);

		assertNotNull(savedRegistration);
		assertEquals(registration.getName(), savedRegistration.getName());
		assertEquals(registration.getEmail(), savedRegistration.getEmail());
		assertEquals(registration.getMobileNo(), savedRegistration.getMobileNo());
		assertEquals(registration.getPassword(), savedRegistration.getPassword());
	}

}
