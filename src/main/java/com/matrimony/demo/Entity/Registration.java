package com.matrimony.demo.Entity;

import jakarta.persistence.Column;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

@Entity
public class Registration {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int regId;

	@Column(name = "Name")
	private String name;

	@Column(name = "Email", unique = true)
	private String email;

	@Column(name = "Mobile No")

	private String mobileNo;

	@Column(name = "Password")
	
	private String password;

	@OneToOne(mappedBy = "registration")

	private Profile profile;

	public Registration() {
		super();
	}
	

	public Registration(int regId, String name, String email, String mobileNo, String password, Profile profile) {
		super();
		this.regId = regId;
		this.name = name;
		this.email = email;
		this.mobileNo = mobileNo;
		this.password = password;
		this.profile = profile;
	}


	public int getRegId() {
		return regId;
	}

	public void setRegId(int regId) {
		this.regId = regId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Registration [regId=" + regId + ", name=" + name + ", email=" + email + ", mobileNo=" + mobileNo
				+ ", password=" + password + "]";
	}
}
