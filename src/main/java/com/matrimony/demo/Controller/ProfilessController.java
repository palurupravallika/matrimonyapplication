package com.matrimony.demo.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.matrimony.demo.Dto.ProfileDTO;
import com.matrimony.demo.Entity.Profile;
import com.matrimony.demo.ServiceImp.ProfileServiceImp;

@RestController
@RequestMapping("/profiles")
public class ProfilessController {


	@Autowired

	ProfileServiceImp profileServiceImp1;
//	
//	 @PutMapping("/{id}")
//	    public ResponseEntity<ProfileDTO> updateProfile(@PathVariable Integer profileId, @RequestBody ProfileDTO profileDTO) {
//	        ProfileDTO updatedProfile = new ProfileDTO();
//	      
//	            return new ResponseEntity<>(updatedProfile, HttpStatus.OK);
//	        
//	    }
//	 
//	 @GetMapping("/search")
//	    public ResponseEntity<List<ProfileDTO>> searchProfiles(@RequestParam(required = false) String gender,
//	                                                           @RequestParam(required = false) String profession) {
//	        List<ProfileDTO> profiles = ProfileServiceImp.searchProfiles(gender, profession);
//	        return new ResponseEntity<>(profiles, HttpStatus.OK);
//	    }
	 

	@PutMapping("/{profileId}")

	public ResponseEntity<Profile> updateProfile(@PathVariable Integer profileId, @RequestBody ProfileDTO profileDTO) {

	
		ProfileDTO updateProfile = profileServiceImp1.updateProfile(profileId, profileDTO);

		return new ResponseEntity(updateProfile, HttpStatus.OK);

	}

	@GetMapping("/{profile_search}")

	public List<Profile> findProfileByGender(@RequestParam String Profile) {

		return profileServiceImp1.findProfileByGender(Profile);

	}
	
//	  @GetMapping("/search")
//	    public List<Profile> searchProfiles(
//	            @RequestParam(required = false) String gender,
//	            @RequestParam(required = false) String caste,
//	            @RequestParam(required = false) String profession) {
//	        if (gender != null && caste != null && profession != null) {
//	            return ProfileServiceImp.findByGenderAndCasteAndProfession(gender, caste, profession);
//	        }
//			return null; 
//	    }


//	@GetMapping("/{profile_search}")
//	public List<ProfileDTO>searchProfiles(@RequestParam(required =false) String gender,
//			                              @RequestParam(required =false) String caste,
//			                              @RequestParam(required =false) String profession) {
//		
//		return profileServiceImp.searchProfiles(gender,caste,profession);
//	
}
