package com.matrimony.demo.Repository;



import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.matrimony.demo.Entity.Registration;
@Repository
public interface RegisterRepository extends JpaRepository<Registration, Integer> {

	
	
	 //Registration login(String email, String password);

	 Registration findByEmail(String email);
		
	

	
	

}
