package com.matrimony.demo.Repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.matrimony.demo.Dto.ProfileDTO;
import com.matrimony.demo.Entity.Profile;
import com.matrimony.demo.Entity.Registration;

public interface ProfileRepository  extends JpaRepository<Profile, Integer>{

	

	List<Profile> findByGender(String gender);

	ProfileDTO save(ProfileDTO profiledto);

	


	//List<ProfileDTO> searchProfiles(String gender, String profession);

	

	

}
