package com.matrimony.demo.Entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;

@Entity
public class Profile {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int profileId;
	@OneToOne(cascade = CascadeType.ALL) // ,mappedBy = "profile")
	@JoinColumn(name = "regId")
	private Registration registration;

	
	private String gender;
	private String height;
	private String weight;
	private String dob;
	private String tob;
	private String birthplace;
	private String caste;
	private String subCaste;
	private String religion;
	private String rasi;
	private String magalic;
	private String education;
	private String profession;
	private String address;

	public Profile() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Profile(String gender, String height, String weight, String dob, String tob, String birthplace, String caste,
			String subCaste, String religion, String rasi, String magalic, String education, String profession,
			String address) {
		super();

		this.gender = gender;
		this.height = height;
		this.weight = weight;
		this.dob = dob;
		this.tob = tob;
		this.birthplace = birthplace;
		this.caste = caste;
		this.subCaste = subCaste;
		this.religion = religion;
		this.rasi = rasi;
		this.magalic = magalic;
		this.education = education;
		this.profession = profession;
		this.address = address;
	}

	public int getProfileId() {
		return profileId;
	}

	public void setProfileId(int profileId) {
		this.profileId = profileId;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getTob() {
		return tob;
	}

	public void setTob(String tob) {
		this.tob = tob;
	}

	public String getBirthPlace() {
		return birthplace;
	}
	public void setBirthPlace(String birthPlace) {
		// TODO Auto-generated method stub
		
	}


	
	public String getCaste() {
		return caste;
	}

	public void setCaste(String caste) {
		this.caste = caste;
	}

	public String getSubCaste() {
		return subCaste;
	}

	public void setSubCaste(String subCaste) {
		this.subCaste = subCaste;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getRasi() {
		return rasi;
	}

	public void setRasi(String rasi) {
		this.rasi = rasi;
	}

	public String getMagalic() {
		return magalic;
	}

	public void setMagalic(String magalic) {
		this.magalic = magalic;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Profile [profileId=" + profileId + ", gender=" + gender + ", height=" + height + ", weight=" + weight
				+ ", dob=" + dob + ", tob=" + tob + ", birthplace=" + birthplace + ", caste=" + caste + ", subCaste="
				+ subCaste + ", religion=" + religion + ", rasi=" + rasi + ", magalic=" + magalic + ", education="
				+ education + ", profession=" + profession + ", address=" + address + "]";
	}

	public Registration getRegistration() {
		return registration;
	}

	public void setRegistration(Registration registration) {
		this.registration = registration;
	}

	public void setName(String name) {
		// TODO Auto-generated method stub
		
	}


	

	
}
