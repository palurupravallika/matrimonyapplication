package com.matrimony.demo.ServiceImp;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.matrimony.demo.Dto.ProfileDTO;
import com.matrimony.demo.Entity.Profile;
import com.matrimony.demo.Repository.ProfileRepository;
import com.matrimony.demo.Service.ProfileService;

@Service
public class ProfileServiceImp implements ProfileService {
	
	  @Autowired
		private static ProfileRepository profileRepository;
	  
		public ProfileServiceImp(ProfileRepository profileRepository) {
		super();
		this.profileRepository = profileRepository;
	}
	 
		public ProfileRepository getProfileRepository() {
			return profileRepository;
		}
	 
		public void setProfileRepository(ProfileRepository profileRepository) {
			this.profileRepository = profileRepository;
		}
	 
	@Override
		public Profile addProfile(Profile profile) {
			return profileRepository.save(profile) ;
		}
	 
	
		@Override
		public List<Profile> findProfileByGender(String gender) {
			return profileRepository.findByGender(gender);
		}
//
//		public static List<Profile> findByGenderAndCasteAndProfession(String gender, String caste, String profession) {
//			// TODO Auto-generated method stub
//			return null;
//		}

//		@Override
//		public ProfileDTO updateProfile(Integer profileId, ProfileDTO profiledto) {
//			// TODO Auto-generated method stub
//			return null;
//		}

		
		
		@Override
		public ProfileDTO updateProfile(Integer profileId, ProfileDTO profiledto) {
			Optional<Profile> opt = profileRepository.findById(profileId);
			Profile profile1 = null;
			if (opt.isPresent()) {

				Profile profile = opt.get();
				profile.setGender(profiledto.getGender());
				profile.setHeight(profiledto.getHeight());
				profile.setWeight(profiledto.getWeight());
				profile.setDob(profiledto.getDob());
				profile.setTob(profiledto.getTob());
				profile.setBirthPlace(profiledto.getBirthPlace());
				profile.setCaste(profiledto.getCaste());
				profile.setSubCaste(profiledto.getSubCaste());
				profile.setReligion(profiledto.getReligion());
				profile.setRasi(profiledto.getRasi());
				profile.setMagalic(profiledto.getMagalic());
				profile.setEducation(profiledto.getEducation());
				
				profile.setProfession(profiledto.getProfession());
				
				profile.setReligion(profiledto.getReligion());
				
				
				profile.setAddress(profiledto.getAddress());
				profile1 = profileRepository.save(profile);

			}
			return profileRepository.save(profiledto);
		}
//		@Override
//		public ProfileDTO updateProfile(Integer profileId, ProfileDTO profiledto) {
//			// TODO Auto-generated method stub
//			return null;
//		}
		
//		@Override
//		public ProfileDTO updateProfile(Integer profileId,ProfileDTO profileDTO) {
//			Profile updatedProfile=profileRepository.findById(profileId)
//					.orElseThrow(()->new RuntimeException("Profile not found id:"+profileId));
//			
//			modelMapper.map(profileDTO,updatedProfile);
//			return modelMapper.map(profileRepository.save(updatedProfile),ProfileDTO.class);
//			
//		}

		
		
	 
		
		}
	 
		
		

	 